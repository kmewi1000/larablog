<?php
	class CommentController extends BaseController{

		public function create() { 
    		$comment=new Comment;
    		return View::make('post.show',array('comment'=>$comment));
    	}

	    public function store($id) { 
	    	$comment=new Comment;
			$comment->nick = Input::get('nick');
			$comment->comment = Input::get('comment');
			if($comment->save()){
					return Redirect::to('post/'.$id);
				}else{
					return 'Error';
				}
	    }
	}
?>