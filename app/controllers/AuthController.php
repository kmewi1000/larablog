<?php

class AuthController extends BaseController{

	public function showLogin(){
		return View::make('home.login');
	}	

	public function LogIn(){

		// Obtenemos los datos del formulario
	    $data = [
	        'username' => Input::get('username'),
	        'password' => Input::get('password')
	    ];
	 
	    // Verificamos los datos
	    if (Auth::attempt($data)) // Como segundo parámetro pasámos el checkbox para sabes si queremos recordar la contraseña
	        {
	            // Si nuestros datos son correctos mostramos la página de inicio
	            return Redirect::to('/admin');
	        }
	    // Si los datos no son los correctos volvemos al login y mostramos un error
	    return Redirect::back()->with('error_message', 'Invalid data')->withInput();
		
	}

	public function LogOut(){
		// Cerramos la sesión
        Auth::logout();
        // Volvemos al login y mostramos un mensaje indicando que se cerró la sesión
        return Redirect::to('/');
	}
}

?>