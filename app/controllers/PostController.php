<?php


	class PostController extends BaseController{

		public function index(){
			$posts=Post::all();
			return View::make('post.posts',array('posts'=>$posts));
	}

	
		public function show($id) { 
			$post=Post::find($id);
			$comments=Comment::all();
			return View::make('post.show',array('post'=>$post,'comments'=>$comments));
     	}
	}
?>