<?php
class AdminController extends BaseController{
	
	public function index(){
        if(Auth::check()){
            $posts=Post::all();
            return View::make('admin.list',array('posts'=>$posts));
        }else{
            return View::make('auth.login');
        }
    }


    public function create() { 
    	$post=new Post;
    	return View::make('admin.save',array('post'=>$post));
    }

    public function store() { 
    	$post=new Post;
		$post->titulo = Input::get('titulo');
		$post->contenido = Input::get('contenido');
		if($post->save()){
				return Redirect::to('admin');
			}else{
				return 'Error';
			}
    }
    public function edit($id) { 
        $post=Post::find($id);
        return View::make('admin.edit')->with('post',$post);
    }
    public function update($id) { 
        $post=Post::find($id);
        $post->titulo = Input::get('titulo');
        $post->contenido = Input::get('contenido');
        if($post->save()){
                return Redirect::to('admin');
            }else{
                return 'Error';
            }
    }
    public function destroy($id) { 
    	$post=Post::find($id);
    	$post->delete();
    	return Redirect::to('admin');
    }
}
?>