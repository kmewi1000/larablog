<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login', 'AuthController@showLogin'); // Mostrar login
Route::post('login', 'AuthController@LogIn'); // Verificar datos
Route::get('logout', 'AuthController@LogOut'); // Finalizar sesión

Route::get('/', 'PostController@index');


Route::get('post/{id}','PostController@show');
Route::post('comment/{id}','CommentController@store');


Route::resource('admin','AdminController',array('before' => 'auth'));
