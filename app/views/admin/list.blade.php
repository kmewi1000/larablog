<html>
<head>
	<title>Larablog</title>
	{{HTML::style('css/style.css')}}
</head>
<body>
	<header>
		<div class="logout"><a href="{{ action('AuthController@LogOut') }}">Log out</a></div>
		<h1>Larablog</h1>
		<h4>(Panel de administración)</h4>
	</header>

	<section>
		<p><img src="img/nuevo.png" width="25"/>&nbsp;&nbsp;{{ link_to ('admin/create', 'Crear nuevo post') }} </p>
		@foreach($posts as $post)
			<article>
				<div class="fechaPost"><div class="bloquefecha"><p>{{ $post->created_at->format('d M')}}</p><p>{{ $post->created_at->format('y')}}</p></div></div>
				<p class="tituloPost">{{ link_to('post/'.$post->id, $post->titulo, array('class'=>'tituloArticulo')) }}</p>
				<p class="contenidoPost">{{$post->contenido}}</p>
				<p class="masPost">{{ link_to('post/'.$post->id, 'Leer más...') }}</p>	
				<hr>
				<span class="adminedit">{{link_to('admin/'.$post->id.'/edit','Editar')}}</span>
					<span class="admindel">
						{{ Form::open(array('url' => 'admin/'.$post->id)) }}
						    {{ Form::hidden("_method", "DELETE") }}
						    {{ Form::submit("Eliminar") }}
					    {{ Form::close() }}
					</span>
				
			</article>
		@endforeach
	</section>
	
<!--	<aside>
			<p>OPCION</p>
	</aside>-->

	<footer>
		<span class="txtfooter">Blog creado por Juan Manuel Cameán Güimil con "Laravel 4.2"</span>
		<span class="redes">
			<img src="../img/facebook.png" width="25">	
			<img src="../img/twitter.png" width="25">	
			<img src="../img/linkedin.png" width="25">
			<img src="../img/googleplus.png" width="25">
		</span>
	</footer>
</body>
</html>