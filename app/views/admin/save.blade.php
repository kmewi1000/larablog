<html>
<head>
	<title>Nueva entrada</title>
	{{HTML::style('css/style.css')}}
</head>
<body>
	<header>
		<div class="logout"><a href="{{ action('AuthController@LogOut') }}">Log out</a></div>
		<h1>Larablog</h1>
		<h4>(Panel de administración)</h4>
	</header>
	<section>
		<article>
			<div class="formcreate">
				{{Form::open(array('url'=>'admin/'.$post->id))}}
					<p>{{ Form::label('titulo', 'Titulo') }}</p>
					<p>{{Form::text('titulo')}}</p>
					<p>{{ Form::label('contenido', 'Contenido') }}</p>
					<p>{{Form::textarea('contenido')}}</p>
					<p>{{Form::submit('Guardar')}}</p>
				{{Form::close()}}
			</div>
		</article>
	</section>
	<footer>
		<span class="txtfooter">Blog creado por Juan Manuel Cameán Güimil con "Laravel 4.2"</span>
		<span class="redes">
			<img src="../img/facebook.png" width="25">	
			<img src="../img/twitter.png" width="25">	
			<img src="../img/linkedin.png" width="25">
			<img src="../img/googleplus.png" width="25">
		</span>
	</footer>
</body>
</html>