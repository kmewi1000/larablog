<html>
<head>
	<title>Larablog</title>
	{{HTML::style('css/style.css')}}
</head>
<body>
	<header>
		<h1>Larablog</h1>
	</header>

	<section>
		
		@foreach($posts as $post)
			<article>
				<div class="fechaPost"><div class="bloquefecha"><p>{{ $post->created_at->format('d M')}}</p><p>{{ $post->created_at->format('y')}}</p></div></div>
				<p class="tituloPost">{{ link_to('post/'.$post->id, $post->titulo, array('class'=>'tituloArticulo')) }}</p>
				<p class="contenidoPost">{{$post->contenido}}</p>
				<p class="masPost">{{ link_to('post/'.$post->id, 'Leer más...') }}</p>	
				<hr>
			</article>
		@endforeach
	</section>
	
<!--	<aside>
			<p>OPCION</p>
	</aside>-->

	<footer>
		<span class="txtfooter">Blog creado por Juan Manuel Cameán Güimil con "Laravel 4.2"</span>
		<span class="redes">
			<img src="img/facebook.png" width="25">	
			<img src="img/twitter.png" width="25">	
			<img src="img/linkedin.png" width="25">
			<img src="img/googleplus.png" width="25">
		</span>
	</footer>
</body>
</html>