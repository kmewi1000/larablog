<html>
<head>
	<title>Larablog</title>
	{{HTML::style('css/style.css')}}
</head>
<body>
	<header>
		<h1>Larablog</h1>
	</header>
	<section>
	<article>
		<div class="fechaPost"><div class="bloquefecha"><p>{{ $post->created_at->format('d M')}}</p><p>{{ $post->created_at->format('y')}}</p></div></div>
		<p class="tituloPostShow">{{$post->titulo}}</p>
		<p class="contenidoPost">{{$post->contenido}}</p>
		<hr>
	</article>
	
	<article>
		<div class="addcomment">
			<p class="itemcomment">Deja tu Comentario:</p>
			
			{{Form::open(array('url'=>'comment/'.$post->id))}}
			<p  class="itemcomment2">Nick / Nombre: {{Form::text('nick')}}</p>
			<p  class="itemcomment2">Comentario: {{Form::textarea('comment',null, ['size' => '105x3'])}}</p>
			<p  class="itemcomment">{{Form::submit('Enviar')}}</p>
			{{Form::close()}}
		</div>		
	</article>
	
	<article>
		<p><b>Comentarios:</b></p>
		@foreach($comments as $comment)
			
				<div class="comentario">
					<p class="txtcomentario">{{$comment->comment}}</p>
					<p class="nickcomentario"><i>{{$comment->nick}}</i></p>
				</div>
		
		@endforeach
		
</section>
	
	<!--<aside>
			<p>OPCION</p>
	</aside>-->

	<footer>
		<div>Blog creado por Juan Manuel Cameán Güimil con "Laravel 4.2"</div>
	</footer>
</body>
</html>