<!doctype html>
<html>
<head>
    <title>LOGIN</title>
    {{HTML::style('css/style.css')}}
</head>
<body>
    <header>
        <div class="logout"><a href="{{ action('AuthController@LogOut') }}">Log out</a></div>
        <h1>Larablog</h1>
        <h4>( Panel de administración )</h4>
    </header>
    <section>
        <article>
            <div class="loginform">
                {{ Form::open(array('url' => 'login')) }}
                
                    @if(Session::has('error_message'))
                        {{ Session::get('error_message') }}
                    @endif
                <!-- if there are login errors, show them here -->
                <p>
                    {{ $errors->first('username') }}
                    {{ $errors->first('password') }}
                </p>

                
                <p class="usuario">
                    {{ Form::label('username', 'Usuario') }}
                    {{ Form::text('username', Input::old('username'), array('placeholder' => 'Nombre de Usuario')) }}
                </p>

                <p class="contrasenha">
                    {{ Form::label('password', 'Password') }}
                    {{ Form::password('password') }}
                </p>

                <p>{{ Form::submit('Submit!') }}</p>
                {{ Form::close() }}
            </div>
        <article>
    </section>
    <footer>
        <span class="txtfooter">Blog creado por Juan Manuel Cameán Güimil con "Laravel 4.2"</span>
        <span class="redes">
            <img src="img/facebook.png" width="25"> 
            <img src="img/twitter.png" width="25">  
            <img src="img/linkedin.png" width="25">
            <img src="img/googleplus.png" width="25">
        </span>
    </footer>