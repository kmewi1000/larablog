<?php
 
use Illuminate\Support\Facades\Hash;
 
class UserTableSeeder extends Seeder {
 
    public function run()
    {
        User::create([
            'username'   => 'admin',
            'password'   =>  Hash::make('secret')
        ]);
    }
 
}